﻿namespace fiskalnaBlagajnaV1
{
    partial class LoginV1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblKorisnik = new System.Windows.Forms.Label();
            this.txtboxKorisnik = new System.Windows.Forms.TextBox();
            this.txtboxLozinka = new System.Windows.Forms.TextBox();
            this.lblLozinka = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblIp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(100, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(506, 45);
            this.label1.TabIndex = 0;
            this.label1.Text = "FISKALNA BLAGAJNA V1 LOGIN";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(-3, 96);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(702, 3);
            this.panel1.TabIndex = 1;
            // 
            // lblKorisnik
            // 
            this.lblKorisnik.AutoSize = true;
            this.lblKorisnik.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblKorisnik.Location = new System.Drawing.Point(269, 111);
            this.lblKorisnik.Name = "lblKorisnik";
            this.lblKorisnik.Size = new System.Drawing.Size(122, 32);
            this.lblKorisnik.TabIndex = 0;
            this.lblKorisnik.Text = "Korisnik :";
            this.lblKorisnik.Click += new System.EventHandler(this.lblKorisnik_Click);
            // 
            // txtboxKorisnik
            // 
            this.txtboxKorisnik.Location = new System.Drawing.Point(100, 146);
            this.txtboxKorisnik.Name = "txtboxKorisnik";
            this.txtboxKorisnik.Size = new System.Drawing.Size(506, 23);
            this.txtboxKorisnik.TabIndex = 2;
            // 
            // txtboxLozinka
            // 
            this.txtboxLozinka.Location = new System.Drawing.Point(100, 224);
            this.txtboxLozinka.Name = "txtboxLozinka";
            this.txtboxLozinka.Size = new System.Drawing.Size(506, 23);
            this.txtboxLozinka.TabIndex = 2;
            // 
            // lblLozinka
            // 
            this.lblLozinka.AutoSize = true;
            this.lblLozinka.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblLozinka.Location = new System.Drawing.Point(269, 189);
            this.lblLozinka.Name = "lblLozinka";
            this.lblLozinka.Size = new System.Drawing.Size(115, 32);
            this.lblLozinka.TabIndex = 0;
            this.lblLozinka.Text = "Lozinka :";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(-3, 310);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(702, 3);
            this.panel2.TabIndex = 1;
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblDateTime.Location = new System.Drawing.Point(12, 325);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(68, 17);
            this.lblDateTime.TabIndex = 0;
            this.lblDateTime.Text = "DateTime";
            this.lblDateTime.Click += new System.EventHandler(this.lblDateTime_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.button1.Location = new System.Drawing.Point(242, 264);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "LOGIN";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(660, 9);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(26, 15);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Exit";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lblIp
            // 
            this.lblIp.AutoSize = true;
            this.lblIp.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblIp.Location = new System.Drawing.Point(599, 325);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(70, 17);
            this.lblIp.TabIndex = 0;
            this.lblIp.Text = "Current IP";
            this.lblIp.Click += new System.EventHandler(this.lblDateTime_Click);
            // 
            // LoginV1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(698, 351);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.lblDateTime);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblLozinka);
            this.Controls.Add(this.txtboxLozinka);
            this.Controls.Add(this.txtboxKorisnik);
            this.Controls.Add(this.lblKorisnik);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginV1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "USER LOGIN V1";
            this.Load += new System.EventHandler(this.LoginV1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblKorisnik;
        private System.Windows.Forms.TextBox txtboxKorisnik;
        private System.Windows.Forms.TextBox txtboxLozinka;
        private System.Windows.Forms.Label lblLozinka;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblIp;
    }
}

