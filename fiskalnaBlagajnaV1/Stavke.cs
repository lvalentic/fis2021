﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace fiskalnaBlagajnaV1
{
    class Stavke
    {
        public int IDstavke { get; set; }
        public string nazivStavke { get; set; }

        public double cijenaStavke { get; set; }

        public Stavke izradiStavku(Stavke stavke)
        {

            

            foreach (var x in popisStavki)
            {
                if (stavke.IDstavke == x.IDstavke)
                {
                    return null;
                }

            }

            this.IDstavke = stavke.IDstavke;
            this.nazivStavke = stavke.nazivStavke;
            this.cijenaStavke = stavke.cijenaStavke;

            popisStavki.Add(stavke);

            return stavke;


        }

        public List<Stavke> popisStavki = new List<Stavke>();

        public bool stavkaPostoji(int id,string nazivStavke)
        {
            
            int naZalihi = popisStavki.Count(p => p.IDstavke == id && p.nazivStavke == nazivStavke);
            if(naZalihi > 0)
            {
                return true;
            }
            return false;
        }

       public void prikaziListu()
        {
            foreach(var x in popisStavki)
            {
                Console.WriteLine(x.IDstavke);
                Console.WriteLine(x.nazivStavke);
                Console.WriteLine(x.cijenaStavke);
            }

           
        }
       
    }
}
